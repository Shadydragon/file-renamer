// Edit these
const file = './input/data.csv';
const input = './input/images/';
const output = './output/';

const limit = 0; // limit the number of lines to parse. set to 0 to set no limit
const in_col = 2;
const out_col = 0;


// inits
const fs = require('fs');

// Task
function defaultTask(cb) {

    try {

        // read contents of the file
        const data = fs.readFileSync(file, 'UTF-8');

        // split the contents by new line
        const lines = data.split(/\r?\n/);

        // print all lines
        lines.forEach((line, index) => {

            var data = line.split(','); // parse the line of the csv

            // check if the data variable exists and has data

            if (limit !== 0 && index > limit - 1) {lines.length = index + 1;} // Behaves like `break`

            if (data) {

                // if the in_col contains a ; it means this is a batch
                if (data[in_col].includes(';')) {

                    console.log('parsing batch '+data[in_col]);

                    // Split the batch out into an array
                    var batch = data[in_col].split(';;;');

                    // loop through the array
                    batch.forEach((element, index) => {
                        // check the file exists
                        if (fs.existsSync(input+element+'.png')){

                            console.log('Renaming ' + input+element+'.png' + ' to ' + output+data[out_col]+'-'+index+'.png');

                            // copy the file to the new destination with the new name
                            fs.copyFileSync(input+element+'.png', output+data[out_col]+'-'+index+'.png');

                        } else {
                            console.log('Error '+ input+element+'.png not found')
                        }
                    });

                    console.log('END parsing batch '+data[in_col]);


                } else {

                    // check the file exists
                    if (fs.existsSync(input+data[in_col]+'.png')) {
                        console.log('Renaming '+data[in_col]+ '.png to '+data[out_col]+'.png');

                        // copy the file to the new destination with the new name
                        fs.copyFileSync(input+data[in_col]+'.png', output+data[out_col]+'.png');
                    } else {
                       console.log('unable to find file ' + data[in_col]);
                    }

                }

            }

        });

    } catch (err) {
        console.error(err);
    }

    cb();

};

exports.default = defaultTask;
